class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "hello, world! - WEB5 - Michael Vornes "
  end

  def goodbye
    render html: "goodbye, world! - WEB5 - Michael Vornes"
  end
end
